# pySplash

## Prerequisites
 * Python 3.7+
 
## Usage

Change to an awesome random wallpaper
```shell
pySplash
```

Change to a random wallpaper by search term
```shell
pySplash nature,water
```

Change to an wallpaper from an image url. 
I recommend using api calls from https://source.unsplash.com

Example:
```shell
pySplash https://source.unsplash.com/1920x1080/daily
```